function validarForma(forma)
{
   var usuario=forma.usuario;
	if(usuario.value===""||usuario.value==="Escribir usuario")
	{
		alert("Debe proporcionar nombre de usuario");
		usuario.focus();
		usuario.select();
		return false;
	}

	var password=forma.password;
	if(password.value===""||password.value.length<3)
	{
		alert("Debe proporcionar un password de almenos 3 caracteres");
		password.focus();
		password.select();
		return false;
	}

	//Validación de checkbox
	var tecnologias=forma.tecno;
	var checkSeleccionado=false;
	for(var i=0; i<tecnologias.length;i++)
	{
		if(tecnologias[i].checked)
		{
			checkSeleccionado=true;
		}
	}
	if(!checkSeleccionado)
	{
		alert("Debe seleccionar una tecnología");
		return false;
	}
	//Validación de checkbox
	var tecnologias=forma.IDE;
	var checkSeleccionado=false;
	for(var i=0; i<tecnologias.length;i++)
	{
		if(tecnologias[i].checked)
		{
			checkSeleccionado=true;
		}
	}
	tecnologias=forma.IDEotro;
	var otroide=document.getElementById('inputOtroIDE');
	if(tecnologias.checked)
	{
		if(otroide.value.length>2)
		{
			checkSeleccionado=true;
		}
	}
	if(!checkSeleccionado)
	{
		alert("Debe seleccionar un IDE");
		return false;
	}

	//Validación de genero
	var generos=forma.genero;
	var radioSeleccionado=false;

	for(var i=0;i<generos.length; i++)
	{
		if(generos[i].checked)
		{
			radioSeleccionado=true;
		}
	}
	if(!radioSeleccionado)
	{
		alert("Debe seleccionar un genero");
		return false;
	}

	//Validación de ocupación
	var ocupacion=forma.ocupacion;
	if(ocupacion.value=="")
	{
		alert("Debe introducir ocupación");
		return false;
	}
	return true;
}


function showInputIDE()
{
   var selectIDE = document.getElementById('selectOtroIDE');
   var inputIDE = document.getElementById('inputOtroIDE');
   if(selectIDE.checked) 
   {
      //alert("come kk");
      inputIDE.style.display = "block";
   } 
   else 
   {
      inputIDE.style.display = "none";
   }
};

