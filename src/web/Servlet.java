package web;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/Servlet")
public class Servlet extends HttpServlet
{
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {


        // get response writer
        PrintWriter out = response.getWriter();




        // read form fields
        String usuario = request.getParameter("usuario");
        String password = request.getParameter("password");
        String tecnologia[] = request.getParameterValues("tecno");
        String ide[] = request.getParameterValues("IDE");
        String ideotro = request.getParameter("IDEotro");
        String genero = request.getParameter("genero");
        String ocupacion = request.getParameter("ocupacion");
        String musica[] = request.getParameterValues("musica");
        String comentario = request.getParameter("comentarios");


        out.print("<html>");
        out.print("<head>");
        out.print("<title>Resultado servlet</title>");
        out.print("</head>");
        out.print("<body>");
        out.print("<h1>Andrade Olvera David</h1>");
        out.print("<h2>Parametros procesados por el SERVLET</h2>");
        out.print("<table borde = '1'>");
        //usuario
        out.print("<tr>");
        out.print("<td>");
        out.print("Usuario:");
        out.print("</td>");
        out.print("<td>");
        out.print(usuario);
        out.print("</td>");
        out.print("</tr>");
        //password
        out.print("<tr>");
        out.print("<td>");
        out.print("Password:");
        out.print("</td>");
        out.print("<td>");
        out.print(password);
        out.print("</td>");
        out.print("</tr>");
        //tecnologias
        out.print("<tr>");
        out.print("<td>");
        out.print("Tecnologias:");
        out.print("</td>");
        out.print("<td>");
        for(String value:tecnologia)
        {
            out.print(value);
            out.print("/");
        }
        out.print("</td>");
        out.print("</tr>");
        //ide
        out.print("<tr>");
        out.print("<td>");
        out.print("IDE:");
        out.print("</td>");
        out.print("<td>");
        for(String id:ide)
        {
            out.print(id);
            out.print("/");
        }
        out.print("</td>");
        out.print("<tr>");
        out.print("<td>");
        out.print("Otro IDE:");
        out.print("</td>");
        out.print("<td>");
        out.print(ideotro);
        out.print("</td>");
        out.print("</tr>");
        out.print("</tr>");
        //genero
        out.print("<tr>");
        out.print("<td>");
        out.print("Genero:");
        out.print("</td>");
        out.print("<td>");
        out.print(genero);
        out.print("</td>");
        out.print("</tr>");
        //ocupacion
        out.print("<tr>");
        out.print("<td>");
        out.print("Ocupacion:");
        out.print("</td>");
        out.print("<td>");
        out.print(ocupacion);
        out.print("</td>");
        out.print("</tr>");
        //comentario
        out.print("<tr>");
        out.print("<td>");
        out.print("Comentario:");
        out.print("</td>");
        out.print("<td>");
        out.print(comentario);
        out.print("</td>");
        out.print("</tr>");





        out.print("</body>");
        out.print("</html>");




    }
}
